(function(){
	angular.module('customFilters', [])
		.filter('marked', ['$sce', function($sce){
			return function(input){
				if(typeof input == 'undefined')
					return;
				return $sce.trustAsHtml(marked(input));
			}
		}]);
})();