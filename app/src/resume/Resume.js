(function(){
	'use strict';

	angular.module('resume', ['ngMaterial', 'customFilters', 'customServices']);

})();