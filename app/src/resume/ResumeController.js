(function(){

	angular
		.module('resume')
		.controller('ResumeController', [
			'Page', '$mdSidenav', '$log', '$q',
			ResumeController
		]);

	/**
	* Main Controller
	* @param $scope
	* @param $mdSidenav
	* @constructor
	*/
	function ResumeController(Page, $mdSidenav, $log, $q) {
		var self = this;

		self.selected = null;
		self.pages = [];
		self.selectPage = selectPage;
		self.toggleList = togglePageList;
		self.morphIcon = morphIcon;

		//load all pages
		var page = Page.query({}, function(pages){
			self.pages = [].concat(pages);
			self.selected = pages[0];
		});

		//internal methods

		function morphIcon(page) {
			console.log('morphing ' + page.name);
			var tmpIcon = 'blur_on';
			var originalIcon = page.icon;
			//change icon to a default icon, and change it back to the orignial icon.
			page.icon = tmpIcon;
			setTimeout(function(){
				page.icon = originalIcon;
			}, 100);
		}

		function togglePageList() {
			var pending = $q.when(true);
			pending.then(function(){
				$mdSidenav('left').toggle();
			});
		}

		function selectPage(page) {
			self.selected = angular.isNumber(page) ? $scope.pages[page] : page;
			self.morphIcon(page);
			self.toggleList();
		}
	}

})();