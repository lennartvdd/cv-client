(function(){
	'use strict';

	var baseUrl = "http://cv-server.vdool.nl/api";
	var authToken = ":";

	var customServices = angular.module('customServices', ['ngResource']);
	
	//Pages Service
	customServices.factory('Page', [
		'$resource', 
		function($resource){
			return $resource(baseUrl + '/page/?id=:id', {}, {
				query: {
					method: 'GET', 
					params: {id: ''}, 
					isArray: true,
					headers: { "Authorization": "Basic " + btoa(authToken), "content-type": "application/json" }
				}
			});
		}
	]);
})();